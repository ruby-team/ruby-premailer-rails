require 'gem2deb/rake/spectask'

# Use installed version of rails, default is 5 (fixes test failure with rails6)
require 'rails'
ENV['ACTION_MAILER_VERSION'] = Rails::VERSION::MAJOR.to_s

task :setup do
  unless Dir.exist?('spec/rails_app/tmp')
    Dir.mkdir 'spec/rails_app/tmp'
  end
end

task :run_tests do
  Gem2Deb::Rake::RSpecTask.new do |spec|
   spec.pattern = './spec/**/*_spec.rb'
  end
end

task :cleanup do
  at_exit {
     `rm -rf spec/rails_app/tmp spec/rails_app/log`
  }
end

task :default => [:setup, :run_tests, :cleanup]
